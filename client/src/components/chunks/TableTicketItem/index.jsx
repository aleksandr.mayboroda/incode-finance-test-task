import React from 'react'
import { Typography, Chip, TableCell, TableRow } from '@mui/material'
import { styled } from '@mui/material/styles'
import { useSelector } from 'react-redux'
import { ticketSelectors } from '../../../store/ticket'

import DynamicCell from '../DynamicCell'
import ActionMenu from '../ActionMenu'

const CustomChips = styled(Chip)(({ theme }) => ({
  color: theme.palette.text.secondary,
  borderRadius: theme.shape.borderRadius,
  padding: 0.5,
}))

const TableTicketItem = ({ item }) => {
  const previous = useSelector(ticketSelectors.getPreviousTicket(item))

  const { ticker, exchange, price, change, change_percent } = item
  return (
    <TableRow sx={{ '&:last-child td, &:last-child td': { border: 0 } }} component="tr">
      <TableCell scope="row" sx={{ display: 'flex', alignItems: 'center' }} >
        <CustomChips color="primary" label={ticker} />
        <Typography variant="h4" sx={{ml: 1}}>{exchange}</Typography>
      </TableCell>
      <TableCell align="right">
        <Typography>{price}</Typography>
      </TableCell>
      <DynamicCell currentVal={change} previousVal={previous?.change} />
      <DynamicCell
        currentVal={change_percent}
        previousVal={previous?.change_percent}
      />
      <TableCell align="right">
        <ActionMenu item={item} />
      </TableCell>
    </TableRow>
  )
}

export default TableTicketItem
