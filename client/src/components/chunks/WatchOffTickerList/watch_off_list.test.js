import WatchOffTickerList from '.'
import { render } from '@testing-library/react'
import { Provider } from 'react-redux'
import store from '../../../store'

describe('WatchOffTickerList component testing', () => {
  test('WatchOffTickerList is rendering', () => {
    render(
      <Provider store={store}>
        <WatchOffTickerList />
      </Provider>
    )
  })
})
