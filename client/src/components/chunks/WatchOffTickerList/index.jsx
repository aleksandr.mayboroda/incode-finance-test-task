import { useCallback } from 'react'
import { Box, Typography, ListItem, Chip } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import { ticketSelectors, ticketOperations } from '../../../store/ticket'

const WatchOffTickerList = () => {
  const dispatch = useDispatch()

  const tickers = useSelector(ticketSelectors.getUnWatchedTickers())
  const toggleWatchList = useCallback((name) => {
    dispatch(ticketOperations.toggleUnWatchList(name))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (!tickers.length > 0) return null

  let list = tickers.map((ticker) => (
    <ListItem key={ticker}>
      <Chip
        label={ticker}
        onDelete={() => toggleWatchList(ticker)}
        sx={{
          textTransform: 'uppercase',
        }}
        data-testid="chip"
      />
    </ListItem>
  ))

  return (
    <Box sx={{ display: 'flex', alignItems: 'center', p: 0 }}>
      <Typography>Hidden tickers:</Typography>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          p: 0.5,
          m: 0,
        }}
        component="ul"
      >
        {list}
      </Box>
    </Box>
  )
}

export default WatchOffTickerList
