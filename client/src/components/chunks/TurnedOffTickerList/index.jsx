import { useCallback } from 'react'
import { Box, Typography, ListItem, Chip } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import { ticketSelectors, ticketOperations } from '../../../store/ticket'

const TurnedOffList = () => {
  const dispatch = useDispatch()

  const tickers = useSelector(ticketSelectors.getUnTrackedTickers())
  
  const toggleTurnOff = useCallback((name) => {
    dispatch(ticketOperations.toggleTrackOff(name))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (!tickers.length > 0) return null

  let list = tickers.map((ticker) => (
    <ListItem key={ticker}>
      <Chip
        label={ticker}
        onDelete={() => toggleTurnOff(ticker)}
        sx={{
          textTransform: 'uppercase',
        }}
      />
    </ListItem>
  ))

  return (
    <Box sx={{ display: 'flex', alignItems: 'center', p: 0 }}>
      <Typography>Untracked tickers:</Typography>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          p: 0.5,
          m: 0,
        }}
        component="ul"
      >
        {list}
      </Box>
    </Box>
  )
}

export default TurnedOffList
