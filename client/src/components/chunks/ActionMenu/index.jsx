import { useState, useCallback } from 'react'
import { IconButton, Menu, MenuItem, ListItemIcon } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import { ticketSelectors, ticketOperations } from '../../../store/ticket'

import MoreIcon from '@mui/icons-material/MoreVert'
import IndeterminateCheckBoxIcon from '@mui/icons-material/IndeterminateCheckBox'
import CheckBoxIcon from '@mui/icons-material/CheckBox'

const ActionMenu = ({ item }) => {
  const dispatch = useDispatch()

  const toggleTurnOff = useCallback(() => {
    dispatch(ticketOperations.toggleTrackOff(item.ticker))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const toggleUnWatchList = useCallback(() => {
    dispatch(ticketOperations.toggleUnWatchList(item.ticker))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const isTurnedOff = useSelector(ticketSelectors.checkIsTurnedOff(item.ticker))
  const isInWatchList = useSelector(
    ticketSelectors.checkIsInUnWatchList(item.ticker)
  )

  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  return (
    <>
      <IconButton
        size="large"
        aria-label="more actions"
        edge="end"
        color="inherit"
        onClick={handleClick}
        aria-controls={open ? 'account-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
      >
        <MoreIcon />
      </IconButton>

      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: 'visible',
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            '&:before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              top: 0,
              right: 20,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <MenuItem onClick={toggleTurnOff}>
          {isTurnedOff ? (
            <>
              <ListItemIcon>
                <IndeterminateCheckBoxIcon fontSize="small" />
              </ListItemIcon>
              Untracked
            </>
          ) : (
            <>
              <ListItemIcon>
                <CheckBoxIcon fontSize="small" />
              </ListItemIcon>
              Tracked
            </>
          )}
        </MenuItem>
        <MenuItem onClick={toggleUnWatchList}>
          {isInWatchList ? (
            <>
              <ListItemIcon>
                <IndeterminateCheckBoxIcon fontSize="small" />
              </ListItemIcon>
              Hide
            </>
          ) : (
            <>
              <ListItemIcon>
                <CheckBoxIcon fontSize="small" />
              </ListItemIcon>
              Show
            </>
          )}
        </MenuItem>
      </Menu>
    </>
  )
}

export default ActionMenu
