import DynamicCell from '.'
import { render, screen } from '@testing-library/react'

describe('Testing DynamicCell component', () => {
  test('Smoke test for DynamicCell', () => {
    const props = { currentVal: 555.55, previousVal: 444.444 }
    render(<DynamicCell {...props} />)
  })

  test('Component arrow is positive', () => {
    const props = { currentVal: 555.55, previousVal: 444.444 }
    render(<DynamicCell {...props} />)
    expect(screen.getByTestId('arrowUp')).toBeInTheDocument()
  })

  test('Component arrow is negative', () => {
    const props = { currentVal: 444.44, previousVal: 555.55 }
    render(<DynamicCell {...props} />)
    expect(screen.getByTestId('arrowDown')).toBeInTheDocument()
  })
})
