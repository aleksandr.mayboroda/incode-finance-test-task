import React from 'react'
import { TableCell, Box, Typography } from '@mui/material'

import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward'

const DynamicCell = ({ currentVal, previousVal }) => {
  const returnStatus = (currentVal, previousVal) => {
    let result = { color: '', arrow: null }
    switch (true) {
      case !previousVal || currentVal === previousVal: {
        result = { color: '', arrow: null }
        break
      }
      case currentVal > previousVal: {
        result = {
          color: 'success.main',
          arrow: <ArrowUpwardIcon sx={{ color: 'success.main', mr: 1 }} data-testid="arrowUp" />,
        }
        break
      }
      case currentVal < previousVal: {
        result = {
          color: 'error.main',
          arrow: <ArrowDownwardIcon sx={{ color: 'error.main', mr: 1 }} data-testid="arrowDown" />,
        }
        break
      }
      default: {
        result = { color: '', arrow: null }
        break
      }
    }
    return result
  }

  const status = returnStatus(currentVal, previousVal)

  return (
    <TableCell align="right">
      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}
      >
        {status.arrow}
        <Typography color={status.color}>{currentVal}</Typography>
      </Box>
    </TableCell>
  )
}

export default DynamicCell
