import { useCallback } from 'react'
import { Paper, Box, Select, MenuItem } from '@mui/material'

import { useSelector, useDispatch } from 'react-redux'
import { ticketOperations, ticketSelectors } from '../../../store/ticket'

import TurnedOffTickerList from '../../chunks/TurnedOffTickerList'
import WatchOffTickerList from '../../chunks/WatchOffTickerList'

const delayList = [
  { name: '1 sec', value: '1000' },
  { name: '3 sec', value: '3000' },
  { name: '5 sec', value: '5000' },
  { name: '10 sec', value: '10000' },
]

const Header = () => {
  const dispatch = useDispatch()
  const interval = useSelector(ticketSelectors.getInterval())
  const setInterval = useCallback(
    (ev) => dispatch(ticketOperations.setInterval(+ev.target.value)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )
  const options =
    !delayList.length > 0
      ? null
      : delayList.map(({ name, value }) => (
          <MenuItem key={value} value={value}>
            {name}
          </MenuItem>
        ))

  return (
    <Paper
      sx={{
        display: 'flex',
        mt: 1,
        p: 2,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
      elevation={1}
    >
      <Box>
        <TurnedOffTickerList />
        <WatchOffTickerList />
      </Box>
      <Box>
        <Select
          label="select time interval"
          onChange={setInterval}
          value={interval}
          name="interval"
          variant="outlined"
        >
          {options}
        </Select>
      </Box>
    </Paper>
  )
}

export default Header
