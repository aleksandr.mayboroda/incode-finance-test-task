import React from 'react'
import {
  Typography,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@mui/material'

import TableTicketItem from '../../chunks/TableTicketItem'

const headerList = [
  { name: 'Ticker', id: 1 },
  { name: 'Price', id: 3 },
  { name: 'Change', id: 4 },
  { name: 'Change percent', id: 5 },
  { name: 'Actions', id: 6 },
]

const Tickers = ({ list }) => {
  const tickerList =
    !list.length > 0
      ? null
      : list.map((item) => <TableTicketItem key={item.ticker} item={item} />)

  const thList =
    !headerList.length > 0
      ? null
      : headerList.map((th, index) => (
          <TableCell align={index > 0 ? 'right' : 'inherit'} key={th.id}>
            {th.name}
          </TableCell>
        ))
  return (
    <>
      <Typography sx={{ mt: 4, mb: 2 }} variant="h3" component="div">
        It can be interesting for you
      </Typography>
      <TableContainer component={Paper} elevation={1}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>{thList}</TableRow>
          </TableHead>
          <TableBody>{tickerList}</TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

export default Tickers
