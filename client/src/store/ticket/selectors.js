const getTickets = () => (state) => state.ticket.data.current
const getInterval = () => (state) => state.ticket.interval
const getPreviousTicket = (obj) => state => state.ticket.data.previous.find(el => el.ticker === obj.ticker)
const checkIsTurnedOff = (name) => state => state.ticket.turnedOff.find(el => el === name.toLowerCase())
const checkIsInUnWatchList = (name) => state => !state.ticket.unWatchList.find(el => el === name.toLowerCase())
const getUnTrackedTickers = () => state => state.ticket.turnedOff
const getUnWatchedTickers = () => state => state.ticket.unWatchList

const def = {
  getTickets,
  getInterval,
  getPreviousTicket,
  checkIsTurnedOff,
  checkIsInUnWatchList,
  getUnTrackedTickers,
  getUnWatchedTickers,
}

export default def
