import types from './types'
import reducer from './reducer'
import actions from './actions'

const testState = {
  interval: 0,
  isLoading: false,
  data: {
    current: [],
    previous: [],
  },
  turnedOff: [],
  unWatchList: [],
}

describe('Ticker reducer test', () => {
  const ticker = 'GOOGLE'
  const tickerArray = [
    {
      ticker: 'GOOGL',
      exchange: 'NASDAQ',
      price: 237.08,
      change: 154.38,
      change_percent: 0.1,
      dividend: 0.46,
      yield: 1.18,
      last_trade_time: '2021-04-30T11:53:21.000Z',
    },
    {
      ticker: 'MSFT',
      exchange: 'NASDAQ',
      price: 237.08,
      change: 154.38,
      change_percent: 0.1,
      dividend: 0.46,
      yield: 1.18,
      last_trade_time: '2021-04-30T11:53:21.000Z',
    },
    {
      ticker: 'AMZN',
      exchange: 'NASDAQ',
      price: 237.08,
      change: 154.38,
      change_percent: 0.1,
      dividend: 0.46,
      yield: 1.18,
      last_trade_time: '2021-04-30T11:53:21.000Z',
    },
  ]

  const updatedTickerArray = [
    {
      ticker: 'GOOGL',
      exchange: 'NASDAQ',
      price: 999,
      change: 5555,
      change_percent: 0.555,
      dividend: 0.555,
      yield: 5.55,
      last_trade_time: '2021-04-30T11:53:21.000Z',
    },
    {
      ticker: 'MSFT',
      exchange: 'NASDAQ',
      price: 237.08,
      change: 154.38,
      change_percent: 0.1,
      dividend: 0.46,
      yield: 1.18,
      last_trade_time: '2021-04-30T11:53:21.000Z',
    },
    {
      ticker: 'AMZN',
      exchange: 'NASDAQ',
      price: 237.08,
      change: 154.38,
      change_percent: 0.1,
      dividend: 0.46,
      yield: 1.18,
      last_trade_time: '2021-04-30T11:53:21.000Z',
    },
  ]

  test('IsLoading sets value', () => {
    const newValue = true
    let newState = reducer(testState, actions.setIsLoading(newValue))
    expect(newState.isLoading).toBeTruthy()
  })

  test('Interval is set directly', () => {
    const interval = 20000

    const action = {
      type: types.SET_INTERVAL,
      payload: interval,
    }
    const newState = reducer(testState, action)
    expect(newState.interval).toBe(interval)
  })

  test('Interval is set by action', () => {
    const interval = 30000
    const newState = reducer(testState, actions.setInterval(interval))
    expect(newState.interval).toBe(interval)
  })

  test('Add ticker to Untracked', () => {
    const newState = reducer(testState, actions.toggleTrackOff(ticker))
    expect(newState.turnedOff).toContain(ticker.toLowerCase()) //ticker to lower case must have
  })

  test('Remove ticker from Untracked', () => {
    let newState = reducer(testState, actions.toggleTrackOff(ticker))
    newState = reducer(newState, actions.toggleTrackOff(ticker))
    expect(newState.turnedOff).not.toContain(ticker) //ticker to lower case must have
  })

  test('Add ticker to Unwatched', () => {
    const newState = reducer(testState, actions.toggleUnWatchList(ticker))
    expect(newState.unWatchList).toContain(ticker.toLowerCase()) //ticker to lower case must have
  })

  test('Remove ticker from Unwatched', () => {
    let newState = reducer(testState, actions.toggleUnWatchList(ticker))
    newState = reducer(newState, actions.toggleUnWatchList(ticker))
    expect(newState.unWatchList).not.toContain(ticker) //ticker to lower case must have
  })

  test('Set tickers array', () => {
    const newState = reducer(testState, actions.setTicket(tickerArray))
    expect(newState.data.current).toBe(tickerArray)
  })

  test('Ticker is not shown on monitor', () => {
    const tick = tickerArray[0].ticker
    let newState = reducer(testState, actions.toggleUnWatchList(tick))
    newState = reducer(newState, actions.setTicket(tickerArray))
    expect(newState.data.current).not.toContain(tickerArray[0])
  })

  test('First ticker is really untracked', () => {
    const tick = tickerArray[0].ticker
    let newState = {}
    newState = reducer(testState, actions.toggleTrackOff(tick))
    newState = reducer(newState, actions.setTicket(updatedTickerArray))
    newState = reducer(newState, actions.setTicket(tickerArray))
    expect(newState.data.current[0].price).toBe(updatedTickerArray[0].price)
  })
})
