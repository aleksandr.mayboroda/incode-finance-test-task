const SET_TICKET = 'finance_task/ticket/SET_TICKET'
const SET_IS_LOADING = 'finance_task/ticket/SET_IS_LOADING'
const SET_INTERVAL = 'finance_task/ticket/SET_INTERVAL'

const TOGGLE_TURN_OFF = 'finance_task/ticket/TOGGLE_TURN_OFF'
const TOGGLE_WATCH_LIST = 'finance_task/ticket/TOGGLE_WATCH_LIST'

const def = {
  SET_TICKET,
  SET_IS_LOADING,
  SET_INTERVAL,
  TOGGLE_TURN_OFF,
  TOGGLE_WATCH_LIST,
}

export default def
