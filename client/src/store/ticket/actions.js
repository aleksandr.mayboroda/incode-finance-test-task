import types from './types'

const setTicket = (array) => ({
  type: types.SET_TICKET,
  payload: array,
})

const setIsLoading = (value) => ({
  type: types.SET_IS_LOADING,
  payload: value,
})

const setInterval = (value) => ({
  type: types.SET_INTERVAL,
  payload: value,
})

const toggleTrackOff = (name) => ({
  type: types.TOGGLE_TURN_OFF,
  payload: name,
})

const toggleUnWatchList = (name) => ({
  type: types.TOGGLE_WATCH_LIST,
  payload: name,
})

const def = {
  setTicket,
  setIsLoading,
  setInterval,
  toggleTrackOff,
  toggleUnWatchList,
}

export default def
