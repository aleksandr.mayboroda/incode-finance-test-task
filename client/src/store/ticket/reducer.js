import types from './types'

const initialState = {
  interval: +localStorage.getItem('interval') || 5000,
  isLoading: false,
  data: {
    current: [],
    previous: [],
  },
  turnedOff: JSON.parse(localStorage.getItem('turnedOff')) || [],
  unWatchList: JSON.parse(localStorage.getItem('unWatchList')) || [],
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_INTERVAL: {
      localStorage.setItem('interval', action.payload)
      return { ...state, interval: action.payload }
    }
    case types.SET_IS_LOADING: {
      return { ...state, isLoading: action.payload }
    }
    case types.SET_TICKET: {
      let previous = state.data.current
      let current = action.payload

      //untrack - не должен обновляться, т.е. показания стабильно те же
      if (state.turnedOff.length > 0) {
        state.turnedOff.forEach((ticker) => {
          const currentIndex = previous.findIndex(
            (item) => item.ticker.toLowerCase() === ticker
          )
          if (currentIndex > -1) {
            current[currentIndex] = previous[currentIndex]
          }
        })
      }

      //hide from unwatched list
      if (state.unWatchList.length > 0) {
        state.unWatchList.forEach((ticker) => {
          previous = previous.filter(
            (item) => item.ticker.toLowerCase() !== ticker
          )
          current = current.filter(
            (item) => item.ticker.toLowerCase() !== ticker
          )
        })
      }

      const data = { current, previous }
      return { ...state, isLoading: false, data }
    }
    case types.TOGGLE_TURN_OFF: {
      const name = action.payload.toLowerCase()
      const exists = state.turnedOff.find((el) => el === name)
      const newList = exists
        ? state.turnedOff.filter((el) => el !== name)
        : [...state.turnedOff, name]
      localStorage.setItem('turnedOff', JSON.stringify(newList))
      return { ...state, turnedOff: newList }
    }
    case types.TOGGLE_WATCH_LIST: {
      const name = action.payload.toLowerCase()
      const exists = state.unWatchList.find((el) => el === name)
      const newList = exists
        ? state.unWatchList.filter((el) => el !== name)
        : [...state.unWatchList, name]
      localStorage.setItem('unWatchList', JSON.stringify(newList))
      return { ...state, unWatchList: newList }
    }

    default:
      return state
  }
}

export default reducer
