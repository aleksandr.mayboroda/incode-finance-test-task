import { useEffect, useRef, useCallback } from 'react'
import { Container } from '@mui/system'
import io from 'socket.io-client'

import { useSelector, useDispatch } from 'react-redux'
import { ticketSelectors, ticketOperations } from './store/ticket'

import Header from './components/blocks/Header'
import Tickers from './components/blocks/Tickers'

function App() {
  const socketRef = useRef()
  const dispatch = useDispatch()

  const interval = useSelector(ticketSelectors.getInterval())
  const tickers = useSelector(ticketSelectors.getTickets())

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const setTickers = useCallback(newTickers => dispatch(ticketOperations.setTicket(newTickers)),[])

  useEffect(() => {
    socketRef.current = io.connect('/') // 1
    socketRef.current.emit('start', interval) //инициализация начинается с emit, т.е. я запускаю отсуда сокет
    console.log('interval',interval)
    socketRef.current.on('ticker', (tickers) => {
      // прием события происходит по "on"
      console.log('res', tickers)

      // 2
      setTickers(tickers)

      // 3 - unmounting
      return () => socketRef.current.emit('disconnect')
    })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    // console.log('change interval to', interval)
    socketRef.current.emit('change_interval', interval) //инициализация начинается с emit, т.е. я запускаю отсуда 
  },[interval])



  return (
    <Container maxWidth="lg">
      <Header />
      <Tickers list={tickers} />
    </Container>
  )
}

export default App
