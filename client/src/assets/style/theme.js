import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
  palette: {
    // type: 'light',
    primary: {
      main: '#3f51b5',
    },
    secondary: {
      main: '#f50057',
    },
    text: {
      primary: '#202124',
      secondary: '#fff',
    },
    divider: '#e8eaed',
    error: {
      main: '#a50e0e',
    },
    success: {
      main: '#137333',
    },
  },
  typography: {
    fontSize: 1,
    fontWeightLight: 300,
    fontWeightMedium: 500,
    htmlFontSize: 1,
    h3: {
      fontSize: '1.1rem',
      lineHeight: 1.5,
    },
    h4: {
      fontSize: '0.825rem',
    },
    button: {
      fontWeight: 500,
      lineHeight: 0.85,
      letterSpacing: '0.72em',
    },
  },
  shape: {
    borderRadius: 4,
  },
  transitions: {
    duration: {
      enteringScreen: 1000,
      leavingScreen: 1000,
    }
  }
})

export default theme
